package calculadora_sergiogonzález;

import java.util.Scanner;

public class Calculadora_SergioGonzález {
    
    static Scanner scanner=new Scanner(System.in);
    static int opcion=-1;//opción del menú
    static double numero1=0,numero2=0;//Variables de entrada
    
    public static void main(String[] args) {
        
        while(opcion!=0){
            //Try catch para evitar que el programa termine si hay un error
            try{
                do{
                System.out.println("Elige opción:\n"+""
                        +"1.-Sumar\n"
                        +"2.-Restar\n"
                        +"3.-Multiplicar\n"
                        +"4.-Dividir\n"
                        +"5.-Porcentaje\n"
                        +"0.-Salir");
                
                System.out.println("Selecciona una opción de 0 a 5");
                opcion=Integer.parseInt(scanner.nextLine());
                String txt= (opcion<0 || opcion>5) ? "Opción inválida.\n" : "";
                    System.out.print(txt);
                }while(opcion<0 || opcion >5);
                
                switch(opcion){
                    case 1:
                        pideNumeros();
                        System.out.println(numero1+"+"+numero2+"="+(numero1+numero2));
                        break;
                    case 2:
                        pideNumeros();
                        System.out.println(numero1+"-"+numero2+"="+(numero1-numero2));
                        break;
                    case 3:
                        pideNumeros();
                        System.out.println(numero1+"*"+numero2+"="+(numero1*numero2));
                        break;
                    case 4:
                        pideNumeros();
                        System.out.println(numero1+"/"+numero2+"="+(numero1/numero2));
                        break;
                    case 5:
                        pideNumeros();
                        System.out.println(numero1+"%"+numero2+"="+(numero1%numero2));
                        break;
                    case 0:
                        System.out.println("FINALIZANDO PROGRAMA...");
                        break;
                }
                System.out.println("\n");
            }catch(Exception e){
                System.out.println("Error!");
            }
        }
    }
    //Método para recoger las variables de entrada
    public static void pideNumeros(){
        System.out.println("Introduce el primer número");
        numero1=Double.parseDouble(scanner.nextLine());
        
        System.out.println("Introduce el segundo número");
        numero2=Double.parseDouble(scanner.nextLine());
    }
}
